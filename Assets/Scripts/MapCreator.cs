﻿using UnityEngine;
using System.Collections;

public class MapCreator : MonoBehaviour {
	public GameObject mapElementPrefab;
	public GameObject mapTheRoomPrefab;

	public float mapElementWidth, mapelementHeight;
	public CameraFollow mapCamera;

	public DungeonCreator dc;
	private Transform[,] mapElements;

	public GameManager gm;

	private void ClearMap() {
		int cCount = transform.childCount;
		for(int i = 0; i < cCount; i++) {
			Transform toDelete = transform.GetChild(0);

			toDelete.parent = null;
			Destroy(toDelete.gameObject);
		}
	}

	public void CreateMap(Room[,] theDungeon, int width, int height) {
		ClearMap();

		mapElements = new Transform[width, height];

		for(int i = 0; i < width; i++) {
			for(int j = 0; j < height; j++) {
				if(theDungeon[i,j].roomType != RoomType.None) {
					GameObject mapElement = theDungeon[i,j].roomType == RoomType.Starting ? mapTheRoomPrefab : mapElementPrefab;

					GameObject GO = (GameObject)(GameObject.Instantiate(mapElement, transform));
					Transform GOT = GO.transform;
					mapElements[i,j] = GOT;

					if(theDungeon[i,j].roomType == RoomType.Starting)
						SetCameraFollow(GOT);
					else {
						GOT.GetChild(1).GetComponent<SpriteRenderer>().color = theDungeon[i,j].enemyColor;
						GOT.GetChild(0).GetComponent<SpriteRenderer>().color = theDungeon[i,j].weaponColor;
					}

					GOT.parent = transform;
					GOT.localPosition = new Vector2(i * mapElementWidth, j * mapelementHeight);
				}
			}
		}
	}

	public void SetCameraFollow(Transform t) {
		mapCamera.toFollow = t;
	}

	private Transform theRoom, toSwap;
	private Vector3 fr, to;
	//NOTE: i'm in a rush, the code is super dirty!
	public void SwapUp() {
		RoomManager roomManager = GameObject.FindGameObjectWithTag("GameController").GetComponent<RoomManager>();
		theRoom = mapElements[roomManager.x, roomManager.y];
		fr = theRoom.position;
		toSwap = mapElements[roomManager.x, roomManager.y + 1];
		to = toSwap.position;

		mapElements[roomManager.x, roomManager.y + 1] = theRoom;
		mapElements[roomManager.x, roomManager.y] = toSwap;

		Room temp = dc.dungeon[roomManager.x, roomManager.y + 1];

		if(temp.roomType == RoomType.Finishing) {
			gm.ChangeState(GameState.Win);
		}

		dc.dungeon[roomManager.x, roomManager.y + 1] = dc.dungeon[roomManager.x, roomManager.y];
		dc.dungeon[roomManager.x, roomManager.y] = temp;

		this.enabled = true;
	}

	public void SwapRight() {
		RoomManager roomManager = GameObject.FindGameObjectWithTag("GameController").GetComponent<RoomManager>();
		theRoom = mapElements[roomManager.x, roomManager.y];
		fr = theRoom.position;
		toSwap = mapElements[roomManager.x + 1, roomManager.y];
		to = toSwap.position;

		mapElements[roomManager.x + 1, roomManager.y] = theRoom;
		mapElements[roomManager.x, roomManager.y] = toSwap;

		Room temp = dc.dungeon[roomManager.x + 1, roomManager.y];

		if(temp.roomType == RoomType.Finishing) {
			gm.ChangeState(GameState.Win);
		}

		dc.dungeon[roomManager.x + 1, roomManager.y] = dc.dungeon[roomManager.x, roomManager.y];
		dc.dungeon[roomManager.x, roomManager.y] = temp;

		this.enabled = true;
	}

	public void SwapDown() {
		RoomManager roomManager = GameObject.FindGameObjectWithTag("GameController").GetComponent<RoomManager>();
		theRoom = mapElements[roomManager.x, roomManager.y];
		fr = theRoom.position;
		toSwap = mapElements[roomManager.x, roomManager.y - 1];
		to = toSwap.position;

		mapElements[roomManager.x, roomManager.y - 1] = theRoom;
		mapElements[roomManager.x, roomManager.y] = toSwap;

		Room temp = dc.dungeon[roomManager.x, roomManager.y - 1];

		if(temp.roomType == RoomType.Finishing) {
			gm.ChangeState(GameState.Win);
		}

		dc.dungeon[roomManager.x, roomManager.y - 1] = dc.dungeon[roomManager.x, roomManager.y];
		dc.dungeon[roomManager.x, roomManager.y] = temp;

		this.enabled = true;
	}

	public void SwapLeft() {
		RoomManager roomManager = GameObject.FindGameObjectWithTag("GameController").GetComponent<RoomManager>();
		theRoom = mapElements[roomManager.x, roomManager.y];
		fr = theRoom.position;
		toSwap = mapElements[roomManager.x - 1, roomManager.y];
		to = toSwap.position;

		mapElements[roomManager.x - 1, roomManager.y] = theRoom;
		mapElements[roomManager.x, roomManager.y] = toSwap;

		Room temp = dc.dungeon[roomManager.x - 1, roomManager.y];

		if(temp.roomType == RoomType.Finishing) {
			gm.ChangeState(GameState.Win);
		}

		dc.dungeon[roomManager.x - 1, roomManager.y] = dc.dungeon[roomManager.x, roomManager.y];
		dc.dungeon[roomManager.x, roomManager.y] = temp;

		this.enabled = true;
	}

	private const float SWAP_TIME = 0.8f;
	private float timeCollector = 0;
	void Update() {
		timeCollector += Time.deltaTime;

		theRoom.position = Vector3.Lerp(fr, to, timeCollector / SWAP_TIME);
		toSwap.position = Vector3.Lerp(to, fr, timeCollector / SWAP_TIME);
			
		if(timeCollector >= SWAP_TIME) {
			timeCollector = 0;
			this.enabled = false;

			gm.ChangeState(GameState.Spawning);
			//Call room change
		}
	}
}
