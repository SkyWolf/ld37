﻿using UnityEngine;
using System.Collections;

public class Room {
	public RoomType roomType;
	public Color enemyColor;
	public Color weaponColor;

	public bool couldBeFinal;

	public EnemyType[] prototype;

	public Room() { roomType = RoomType.None; }
	public void ChangeRoomType(RoomType _roomType) {
		roomType = _roomType;
	}

	public void InitializeRoom(bool _couldBeFinal) {
		couldBeFinal = _couldBeFinal;
		VarsContainer vars = GameObject.FindGameObjectWithTag("Global").GetComponent<VarsContainer>();

		prototype = vars.roomPrototypes[vars.rand.Next(0, vars.roomPrototypes.Length)];


		switch(roomType) {
			case RoomType.Normal:
				enemyColor = Color.HSVToRGB((float)vars.rand.NextDouble(),1,1);
				weaponColor = Color.HSVToRGB((float)vars.rand.NextDouble(),1,1);
				break;
			case RoomType.Finishing:
				enemyColor = Color.black;
				weaponColor = Color.white;
				break;
			case RoomType.Starting:
				enemyColor = Color.white;
				weaponColor = Color.white;
				break;
		}
	}
}