﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public enum GameState {
	Start,
	Spawning,
	Fight,
	ChangeRoom,
	Win,
	GameOver
}

public class GameManager : MonoBehaviour {
	private GameState currentState;
	private Animator[] vortexes;

	private RoomManager roomManager;
	private VarsContainer vars;

	private CharControl characterController;
	public GameObject pressStart;
	public Animator mapCameraAnimator;
	public GameObject arrows;

	public GameObject gameover;
	public GameObject win;

	void ExitState(GameState state) {
		switch(currentState) {
		case GameState.Start:
			GameObject[] vGOs = GameObject.FindGameObjectsWithTag("Vortex");
			vortexes = new Animator[vGOs.Length];
			for(int i = 0; i < vortexes.Length; i++) {
				vortexes[i] = vGOs[i].GetComponent<Animator>();
			}

			roomManager = GameObject.FindGameObjectWithTag("GameController").GetComponent<RoomManager>();
			vars = GameObject.FindGameObjectWithTag("Global").GetComponent<VarsContainer>();
			characterController = GameObject.FindGameObjectWithTag("Player").GetComponent<CharControl>();

			characterController.enabled = true;
			pressStart.SetActive(false);
			break;
		case GameState.Spawning:
			for(int i = 0; i < vortexes.Length; i++) {
				vortexes[i].ResetTrigger("Spawn");
			}
			break;
		}
	}

	void EnterState(GameState state) {
		switch(currentState) {
		case GameState.Spawning:
			characterController.enabled = true;
			mapCameraAnimator.ResetTrigger("Zoom");
			mapCameraAnimator.SetTrigger("Unzoom");
			Vortex vx = vortexes[0].GetComponent<Vortex>();
			vx.expansion = FullExpansion;
			vx.end = Disappear;
			for(int i = 0; i < vortexes.Length; i++) {
				vortexes[i].SetTrigger("Spawn");
			}
			break;
		case GameState.ChangeRoom:
			//TODO: switch rooms
			characterController.enabled = false;
			mapCameraAnimator.ResetTrigger("Unzoom");
			mapCameraAnimator.SetTrigger("Zoom");

			arrows.GetComponent<ArrowsChecker>().rm = roomManager;
			arrows.SetActive(true);
			break;
		case GameState.Win:
			characterController.enabled = false;
			win.SetActive(true);
			break;
		case GameState.GameOver:
			characterController.enabled = false;
			gameover.SetActive(true);
			break;
		}
	}

	void UpdateState(GameState state) {
		switch(currentState) {
		case GameState.Start:
			if(Input.anyKeyDown)
				ChangeState(GameState.Spawning);
			break;
		case GameState.Fight:
			if(roomManager.monsterContainer.childCount == 0)
				ChangeState(GameState.ChangeRoom);
			break;
		}
	}

	void Update() {
		UpdateState(currentState);
	}

	public void FullExpansion() {
		int x = roomManager.x;
		int y = roomManager.y;
		DungeonCreator theDungeon = roomManager.theDungeon;
		EnemyType[] eProto;
		GameObject enemy;

		Random.InitState(vars.rand.Next());

		if(x > 0 && theDungeon.dungeon[x - 1, y].roomType != RoomType.None) {
			eProto = theDungeon.dungeon[x - 1, y].prototype;
			for(int i = 0; i < eProto.Length; i++) {
				enemy = (GameObject)GameObject.Instantiate(vars.enemies[(int)eProto[i]], roomManager.monsterContainer);
				enemy.GetComponent<Colorizer>().ChangeColor(theDungeon.dungeon[x - 1, y].enemyColor);
				enemy.transform.position = roomManager.leftVortex.position + (Vector3)(Random.insideUnitCircle * roomManager.vortexRadius);
			}
		}
		if(x < theDungeon.width - 1 && theDungeon.dungeon[x + 1, y].roomType != RoomType.None) {
			eProto = theDungeon.dungeon[x + 1, y].prototype;
			for(int i = 0; i < eProto.Length; i++) {
				enemy = (GameObject)GameObject.Instantiate(vars.enemies[(int)eProto[i]], roomManager.monsterContainer);
				enemy.GetComponent<Colorizer>().ChangeColor(theDungeon.dungeon[x + 1, y].enemyColor);
				enemy.transform.position = roomManager.rightVortex.position + (Vector3)(Random.insideUnitCircle * roomManager.vortexRadius);
			}
		}
		if(y > 0 && theDungeon.dungeon[x, y - 1].roomType != RoomType.None) {
			eProto = theDungeon.dungeon[x, y - 1].prototype;
			for(int i = 0; i < eProto.Length; i++) {
				enemy = (GameObject)GameObject.Instantiate(vars.enemies[(int)eProto[i]], roomManager.monsterContainer);
				enemy.GetComponent<Colorizer>().ChangeColor(theDungeon.dungeon[x, y - 1].enemyColor);
				enemy.transform.position = roomManager.downVortex.position + (Vector3)(Random.insideUnitCircle * roomManager.vortexRadius);
			}
		}
		if(y < theDungeon.height - 1 && theDungeon.dungeon[x, y + 1].roomType != RoomType.None) {
			eProto = theDungeon.dungeon[x, y + 1].prototype;
			for(int i = 0; i < eProto.Length; i++) {
				enemy = (GameObject)GameObject.Instantiate(vars.enemies[(int)eProto[i]], roomManager.monsterContainer);
				enemy.GetComponent<Colorizer>().ChangeColor(theDungeon.dungeon[x, y + 1].enemyColor);
				enemy.transform.position = roomManager.topVortex.position + (Vector3)(Random.insideUnitCircle * roomManager.vortexRadius);
			}
		}
	}

	public void Disappear() {
		ChangeState(GameState.Fight);
	}

	public void ChangeState(GameState newState) {
		if(newState == currentState)
			return;
		ExitState(currentState);
		currentState = newState;
		EnterState(currentState);
	}
}
