﻿using System;
using UnityEngine;

[Serializable]
public class SingleW : Weapon
{
	protected override void Shoot(Color c, Vector3 position, Vector3 direction) {
		GameObject bullet = (GameObject)GameObject.Instantiate(bullets[0], position, Quaternion.identity);
		bullet.GetComponent<Colorizer>().ChangeColor(c);
		BulletSimple bs = bullet.GetComponent<BulletSimple>();
		bs.Shoot(direction);
	}
}