﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof(MeshFilter))]
[RequireComponent (typeof(MeshRenderer))]
public class RoomCreator : MonoBehaviour {
	private VarsContainer vars;
	private MeshFilter myMeshFilter;

	public int width { get; private set; }
	public int height { get; private set; }

	public BoxCollider2D top, right, down, left;
	public GameObject vortexPrefab;
	public RoomManager rm;

	private void Initialize() {
		GameObject varsGO = GameObject.FindGameObjectWithTag("Global");
		if(!varsGO)
			Debug.LogError("No \"Global\" object found!");
		else {
			vars = varsGO.GetComponent<VarsContainer>();
			if(!vars) {
				Debug.LogError("\"Global\" had no \"VarsContainer\"");
			}
		}

		myMeshFilter = GetComponent<MeshFilter>();
	}

	public void CreateRoom(int rWidth, int rHeight) {
		if(!vars)
			Initialize();

		width = rWidth;
		height = rHeight;

		int halfW = (int)(width / 2);
		int halfH = (int)(height / 2);

		Tile[] tiles = vars.tiles;
		float totalFrequency = 0;
		for(int t = 0; t < tiles.Length; t++) {
			totalFrequency += tiles[t].frequency;
		}

		top.offset = new Vector2(0, halfH + 5.5f);
		right.offset = new Vector2(halfW + 5.5f, 0);
		down.offset = new Vector2(0, -halfH - 5.5f);
		left.offset = new Vector2(-halfW - 5.5f, 0);

		GameObject vortex;

		vortex = (GameObject)GameObject.Instantiate(vortexPrefab, transform);
		rm.topVortex = vortex.transform;
		rm.topVortex.position = new Vector2(0, halfH - 1.5f);

		vortex = (GameObject)GameObject.Instantiate(vortexPrefab, transform);
		rm.rightVortex = vortex.transform;
		rm.rightVortex.transform.position = new Vector2(halfW - 1.5f, 0);

		vortex = (GameObject)GameObject.Instantiate(vortexPrefab, transform);
		rm.downVortex = vortex.transform;
		rm.downVortex.position = new Vector2(0, -halfH + 1.5f);

		vortex = (GameObject)GameObject.Instantiate(vortexPrefab, transform);
		rm.leftVortex = vortex.transform;
		rm.leftVortex.position = new Vector2(-halfW + 1.5f, 0);

		down.size = top.size = new Vector2(width, 10);
		left.size = right.size = new Vector2(10, height);

		Mesh levelMesh = new Mesh();
		Vector3[] vertices = new Vector3[width * height * 4];
		Vector2[] uvs = new Vector2[width * height * 4];
		int[] triangles = new int[width * height * 2 * 3];
		Vector3[] normals = new Vector3[width * height * 4];

		float cumulativeFrequency;
		float tileRandomizer;
		for(int i = 0; i < width; i++) {
			for(int j = 0; j < height; j++) {
				tileRandomizer = (float)(vars.rand.NextDouble() * totalFrequency);
				cumulativeFrequency = 0;

				int vIndex = (i * height * 4) + (j * 4);
				int tIndex = (i * height * 2 * 3) + (j * 2 * 3);

				for(int t = 0; t < tiles.Length; t++) {
					cumulativeFrequency += tiles[t].frequency;
					if(tileRandomizer < cumulativeFrequency) {
						vertices[vIndex + 0] = new Vector3((i-halfW) - 0.5f, (j-halfH) - 0.5f, 0);
						vertices[vIndex + 1] = new Vector3((i-halfW) - 0.5f, (j-halfH) + 0.5f, 0);
						vertices[vIndex + 2] = new Vector3((i-halfW) + 0.5f, (j-halfH) + 0.5f, 0);
						vertices[vIndex + 3] = new Vector3((i-halfW) + 0.5f, (j-halfH) - 0.5f, 0);

						triangles[tIndex + 0] = vIndex + 0;
						triangles[tIndex + 1] = vIndex + 1;
						triangles[tIndex + 2] = vIndex + 3;
						triangles[tIndex + 3] = vIndex + 1;
						triangles[tIndex + 4] = vIndex + 2;
						triangles[tIndex + 5] = vIndex + 3;

						//TODO: reduce overhead
						Rect tempRect = tiles[t].graphics.textureRect;
						int tWidth = tiles[t].graphics.texture.width;
						int tHeight = tiles[t].graphics.texture.height;
						Rect spriteRect = new Rect(tempRect.x / tWidth, tempRect.y / tHeight, tempRect.width / tWidth, tempRect.height / tHeight);

						uvs[vIndex + 0] = new Vector2(spriteRect.x, spriteRect.y);
						uvs[vIndex + 1] = new Vector2(spriteRect.x, spriteRect.y + spriteRect.height);
						uvs[vIndex + 2] = new Vector2(spriteRect.x + spriteRect.width, spriteRect.y + spriteRect.height);
						uvs[vIndex + 3] = new Vector2(spriteRect.x + spriteRect.width, spriteRect.y);

						normals[vIndex + 0] = -Vector3.forward;
						normals[vIndex + 1] = -Vector3.forward;
						normals[vIndex + 2] = -Vector3.forward;
						normals[vIndex + 3] = -Vector3.forward;

						//Old Method: 
						/*GameObject newGO = new GameObject("(" + i + ","+ j + ")");
						Transform newTra = newGO.transform;
						SpriteRenderer newSR = newGO.AddComponent<SpriteRenderer>();

						newSR.sprite = tiles[t].graphics;
						newSR.sharedMaterial = vars.spriteMaterial;
						newTra.parent = transform;
						newTra.position = new Vector2(i - halfW, j - halfH);*/

						t = tiles.Length;
					}
				}
			}
		}

		levelMesh.vertices = vertices;
		levelMesh.uv = uvs;
		levelMesh.triangles = triangles;
		levelMesh.normals = normals;

		myMeshFilter.mesh = levelMesh;
	}
}