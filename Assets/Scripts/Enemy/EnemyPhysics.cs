﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof(EnemyParameters))]
public class EnemyPhysics : MonoBehaviour {
	private Rigidbody2D myRigidbody;
	private EnemyParameters eParameters;

	void Start() {
		myRigidbody = GetComponent<Rigidbody2D>();
		eParameters = GetComponent<EnemyParameters>();
	}

	void OnTriggerEnter2D(Collider2D col) {
		if(col.tag == "Bullet") {
			float bullethue = col.GetComponent<Colorizer>().GetHue();
			float bDamage = col.GetComponent<IBullet>().GetDamage();
			float enemyhue = GetComponent<Colorizer>().GetHue();

			if(bullethue > 0.5f)
				bullethue = bullethue - 0.5f;
			if(enemyhue > 0.5f)
				enemyhue = enemyhue - 0.5f;

			Vector3 direction = transform.position - col.transform.position;
			myRigidbody.AddForce(direction * 50);

			bDamage = bDamage + ((bDamage / 2) * Mathf.Abs(bullethue - enemyhue) * 2);
			eParameters.health -= bDamage;

			col.enabled = false;
			Destroy(col.gameObject);
		}
	}
}
