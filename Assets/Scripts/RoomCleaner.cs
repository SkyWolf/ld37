﻿using UnityEngine;
using System.Collections;

public class RoomCleaner : MonoBehaviour {
	// Update is called once per frame
	void Update () {
		if(Input.GetKeyDown(KeyCode.H)) {
			int count = transform.childCount;
			for(int i = 0; i < count; i++) {
				Transform t = transform.GetChild(0);
				t.parent = null;
				Destroy(t.gameObject);
			}
		}
	}
}
