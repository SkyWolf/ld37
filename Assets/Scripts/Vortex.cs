﻿using UnityEngine;
using System.Collections;

public delegate void DoSomething();
public class Vortex : MonoBehaviour {
	public DoSomething expansion;
	public DoSomething end;

	public void FullExpansion() { 
		if(expansion != null)
			expansion();
	}

	public void End() { 
		if(end != null)
			end();
	}
}