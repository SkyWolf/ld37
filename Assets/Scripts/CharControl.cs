﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class CharControl : MonoBehaviour {
	public GameManager gm;
	public float maxSpeed = 15;
	private float maxSpeedSqr;
	public float acceleration = 50;

	public float lightsMinSpeed = 10;
	public float lightsRangeSpeed = 1000;

	private Vector2 movement;
	private Vector2 speed;
	private Vector2 lookDirection;

	public SpriteRenderer bodyR;
	public SpriteRenderer armR;
	public Light lightone;
	public Light lighttwo;

	public Transform arm;
	public Transform lights;
	public Transform shootFrom;

	private Rigidbody2D myRigidbody;

	private bool canShoot = true;
	private float timeCollector = float.MaxValue;

	public Image healthBar;
	public float healthbarMax;

	public float maxHealth;
	private float health;

	private Color weaponColor;

	public bool isInvincible { get; private set; }
	private float invincibilityCollector = 0;
	public float invincibilityDuration;

	void Start() {
		myRigidbody = GetComponent<Rigidbody2D>();
		maxSpeedSqr = maxSpeed * maxSpeed;

		health = maxHealth;
	}

	private void UpdateMovement() {
		//TODO: implement controller
		movement = Vector2.zero;
		if(Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.LeftArrow)) {
			movement.x -= 1;
		}
		if(Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow)) {
			movement.x += 1;
		}
		if(Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.DownArrow)) {
			movement.y -= 1;
		}
		if(Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.UpArrow)) {
			movement.y += 1;
		}
		movement.Normalize();

		if(Mathf.Abs(movement.x) < 0.2f) {
			if(Mathf.Abs(speed.x) < 0.1f)
				speed.x = 0;
			else
				speed.x += -Mathf.Sign(speed.x) * acceleration * Time.deltaTime;
		}
		if(Mathf.Abs(movement.y) < 0.2f) {
			if(Mathf.Abs(speed.y) < 0.1f)
				speed.y = 0;
			else
				speed.y += -Mathf.Sign(speed.y) * acceleration * Time.deltaTime;
		}
		speed += movement * acceleration * Time.deltaTime;

		speed = Vector2.ClampMagnitude(speed, maxSpeed);
	}

	private void UpdateArm() {
		Vector3 wPoint = Camera.main.ScreenToWorldPoint(Input.mousePosition);
		wPoint.z = transform.position.z;
		lookDirection = wPoint = transform.position - wPoint;
		arm.rotation = Quaternion.LookRotation(wPoint);
	}

	private void UpdateLights() {
		lights.Rotate(Vector3.forward, ((speed.sqrMagnitude / maxSpeedSqr) * lightsRangeSpeed + lightsMinSpeed) * Time.deltaTime);
	}

	void Update() {
		invincibilityCollector += Time.deltaTime;
		if(invincibilityCollector >= invincibilityDuration) {
			invincibilityCollector = 0;
			isInvincible = false;

			Color c = bodyR.color;
			c.a = 1;
			bodyR.color = c;
		}

		if(isInvincible) {
			Color c = bodyR.color;
			c.a = .5f;
			bodyR.color = c;
		}

		UpdateMovement();
		UpdateArm();
		UpdateLights();

		//TODO: change this
		GameObject.FindGameObjectWithTag("Global").GetComponent<VarsContainer>().weapons[0].Update(weaponColor, shootFrom.position, -lookDirection.normalized);
	}

	void FixedUpdate() {
		myRigidbody.position += speed * Time.fixedDeltaTime;
	}

	public void SetWeaponColor(float hue) {
		weaponColor = Color.HSVToRGB(hue, 1, 1);
		bodyR.color = weaponColor;
		armR.color = weaponColor;
		lightone.color = weaponColor;
		lighttwo.color = weaponColor;
	}

	void OnEnable() {
		SetInvincibilityFrames();
	}

	public void SetInvincibilityFrames() {
		isInvincible = true;
		invincibilityCollector = 0;

		bodyR.color = weaponColor;
		armR.color = weaponColor;
	}

	void OnTriggerEnter2D(Collider2D col) {
		if(this.enabled == false || isInvincible)
			return;
		if(col.tag == "Bullet") {
			float bullethue = col.GetComponent<Colorizer>().GetHue();
			float bDamage = col.GetComponent<IBullet>().GetDamage();
			float h,s,v;
			Color.RGBToHSV(weaponColor, out h, out s, out v);
			float enemyhue = h;

			if(bullethue > 0.5f)
				bullethue = bullethue - 0.5f;
			if(enemyhue > 0.5f)
				enemyhue = enemyhue - 0.5f;

			Vector3 direction = transform.position - col.transform.position;
			myRigidbody.AddForce(direction * 50);

			bDamage = bDamage + ((bDamage / 2) * Mathf.Abs(bullethue - enemyhue) * 2);
			health -= bDamage;
			if(health < 0) {
				health = 0;
				gm.ChangeState(GameState.GameOver);
			}

			SetInvincibilityFrames();

			healthBar.rectTransform.localScale = new Vector3(health / maxHealth, healthBar.rectTransform.localScale.y, healthBar.rectTransform.localScale.z);

			col.enabled = false;
			Destroy(col.gameObject);
		}
	}

	void OnCollisionEnter2D(Collision2D col) {
		if(this.enabled == false || isInvincible)
			return;
		if(col.collider.tag == "Enemy") {
			health -= 1;
			if(health < 0) {
				health = 0;
				gm.ChangeState(GameState.GameOver);
			}

			SetInvincibilityFrames();

			healthBar.rectTransform.localScale = new Vector3(health / maxHealth, healthBar.rectTransform.localScale.y, healthBar.rectTransform.localScale.z);
		}
	}
}