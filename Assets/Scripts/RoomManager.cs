﻿using UnityEngine;
using System.Collections;

public class RoomManager : MonoBehaviour {
	public int x { get; private set; }
	public int y { get; private set; }

	public DungeonCreator theDungeon { get; private set; }
	private DifficultyBar diffBar;
	private CharControl characterController;

	public Material roomsMaterial;

	public Transform monsterContainer;
	public Transform topVortex, rightVortex, downVortex, leftVortex;

	public float vortexRadius = 1f;

	public void Initialize(DungeonCreator dungeon, int _x, int _y) {
		diffBar = GameObject.FindGameObjectWithTag("DifficultyBar").GetComponent<DifficultyBar>();
		characterController = GameObject.FindGameObjectWithTag("Player").GetComponent<CharControl>();

		theDungeon = dungeon;
		ChangeCoordinates(_x, _y);
	}

	private void ChangeCoordinates(int _x, int _y) {
		x = _x;
		y = _y;

		float h,s,v;
		float avgEnemyHue = 0;
		float avgWeaponHue = 0;
		int counter = 0;
		if(x > 0 && theDungeon.dungeon[x - 1, y].roomType != RoomType.None) {
			leftVortex.GetChild(0).GetComponent<SpriteRenderer>().color = theDungeon.dungeon[x - 1, y].enemyColor;
			Color.RGBToHSV(theDungeon.dungeon[x - 1, y].enemyColor, out h, out s, out v);
			avgEnemyHue += h;
			Color.RGBToHSV(theDungeon.dungeon[x - 1, y].weaponColor, out h, out s, out v);
			avgWeaponHue += h;
			
			counter++;
		}
		else
			leftVortex.GetChild(0).GetComponent<SpriteRenderer>().color = new Color(0,0,0,0);
		
		if(x < theDungeon.width - 1 && theDungeon.dungeon[x + 1, y].roomType != RoomType.None) {
			rightVortex.GetChild(0).GetComponent<SpriteRenderer>().color = theDungeon.dungeon[x + 1, y].enemyColor;
			Color.RGBToHSV(theDungeon.dungeon[x + 1, y].enemyColor, out h, out s, out v);
			avgEnemyHue += h;
			Color.RGBToHSV(theDungeon.dungeon[x + 1, y].weaponColor, out h, out s, out v);
			avgWeaponHue += h;

			counter++;
		}
		else
			rightVortex.GetChild(0).GetComponent<SpriteRenderer>().color = new Color(0,0,0,0);

		if(y > 0 && theDungeon.dungeon[x, y - 1].roomType != RoomType.None) {
			downVortex.GetChild(0).GetComponent<SpriteRenderer>().color = theDungeon.dungeon[x, y - 1].enemyColor;
			Color.RGBToHSV(theDungeon.dungeon[x, y - 1].enemyColor, out h, out s, out v);
			avgEnemyHue += h;
			Color.RGBToHSV(theDungeon.dungeon[x, y - 1].weaponColor, out h, out s, out v);
			avgWeaponHue += h;

			counter++;
		}
		else
			downVortex.GetChild(0).GetComponent<SpriteRenderer>().color = new Color(0,0,0,0);

		if(y < theDungeon.height - 1 && theDungeon.dungeon[x, y + 1].roomType != RoomType.None) {
			topVortex.GetChild(0).GetComponent<SpriteRenderer>().color = theDungeon.dungeon[x, y + 1].enemyColor;
			Color.RGBToHSV(theDungeon.dungeon[x, y + 1].enemyColor, out h, out s, out v);
			avgEnemyHue += h;
			Color.RGBToHSV(theDungeon.dungeon[x, y + 1].weaponColor, out h, out s, out v);
			avgWeaponHue += h;

			counter++;
		}
		else
			topVortex.GetChild(0).GetComponent<SpriteRenderer>().color = new Color(0,0,0,0);

		avgWeaponHue /= counter;
		avgEnemyHue /= counter;

		diffBar.SetWepColor(avgWeaponHue);
		diffBar.SetEnemyColor(avgEnemyHue);

		characterController.SetWeaponColor(avgWeaponHue);
		//TODO: change this?
		roomsMaterial.color = Color.HSVToRGB(avgEnemyHue, 0.2f, 1);
	}

	public bool GoRight() {
		if(x < theDungeon.width - 1 && theDungeon.dungeon[x + 1, y].roomType != RoomType.None) {
			ChangeCoordinates(x + 1, y);
		}
		else
			return false;
		return true;
	}
	public bool GoDown() {
		if(y < theDungeon.height - 1 && theDungeon.dungeon[x, y - 1].roomType != RoomType.None) {
			ChangeCoordinates(x, y - 1);
		}
		else
			return false;
		return true;
	}
	public bool GoLeft() {
		if(x > 0 && theDungeon.dungeon[x - 1, y].roomType != RoomType.None) {
			ChangeCoordinates(x - 1, y);
		}
		else
			return false;
		return true;
	}
	public bool GoUp() {
		if(y > 0 && theDungeon.dungeon[x, y + 1].roomType != RoomType.None) {
			ChangeCoordinates(x, y + 1);
		}
		else
			return false;
		return true;
	}
}