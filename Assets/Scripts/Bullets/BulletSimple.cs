﻿using UnityEngine;
using System.Collections;

public class BulletSimple : MonoBehaviour, IBullet {
	public float speed;
	public float travelDistance;
	public float timeToDie;

	private Vector3 travelDirection = new Vector3(0.7f, 0.7f);
	private float distanceCollector = 0;
	private float timeCollector = 0;
	public float damage = 1;

	void Update() {
		float deltaDistance = speed * Time.deltaTime;

		distanceCollector += deltaDistance;
		if(distanceCollector >= travelDistance) {
			timeCollector += Time.deltaTime;
			if(timeCollector >= timeToDie)
				Destroy(gameObject);
			float lerp = Mathf.Lerp(0, 1, timeCollector / timeToDie);
			float oneminuslerp = 1 - lerp;
			transform.position += (travelDirection * deltaDistance * oneminuslerp) - (Vector3.up * deltaDistance * lerp / 3);
			transform.localScale = Vector3.one * oneminuslerp;
		}
		else
			transform.position += travelDirection * deltaDistance;
	}

	public void Shoot(Vector3 direction) {
		travelDirection = direction;
		this.enabled = true;
	}

	public float GetDamage() {
		return damage; 
	}
}
