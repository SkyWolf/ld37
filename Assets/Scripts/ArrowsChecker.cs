﻿using UnityEngine;
using System.Collections;

public class ArrowsChecker : MonoBehaviour {
	// Update is called once per frame
	public GameObject up, right, down, left;
	public RoomManager rm;
	public MapCreator mc;

	void OnEnable() {
		if(rm) {
			int x = rm.x;
			int y = rm.y;
			DungeonCreator d = rm.theDungeon;

			if(x == 0 || d.dungeon[x - 1, y].roomType == RoomType.None)
				left.SetActive(false);
			else
				left.SetActive(true);
			
			if(x == d.width - 1 || d.dungeon[x + 1, y].roomType == RoomType.None)
				right.SetActive(false);
			else
				right.SetActive(true);

			if(y == 0 || d.dungeon[x, y - 1].roomType == RoomType.None)
				down.SetActive(false);
			else
				down.SetActive(true);

			if(y == d.height - 1 || d.dungeon[x, y + 1].roomType == RoomType.None)
				up.SetActive(false);
			else
				up.SetActive(true);
		}
	}

	void Update () {
		if(Input.GetKeyDown(KeyCode.Mouse0)) {
			Camera c = transform.parent.GetComponent<Camera>();
			Ray r = c.ViewportPointToRay(c.ScreenToViewportPoint(Input.mousePosition));
			RaycastHit h;
			if(Physics.Raycast(r, out h)) {
				if(h.collider == up.GetComponent<BoxCollider>()) {
					gameObject.SetActive(false);
					mc.SwapUp();
					rm.GoUp();
				}
				if(h.collider == down.GetComponent<BoxCollider>()) {
					gameObject.SetActive(false);
					mc.SwapDown();
					rm.GoDown();
				}
				if(h.collider == left.GetComponent<BoxCollider>()) {
					gameObject.SetActive(false);
					mc.SwapLeft();
					rm.GoLeft();
				}
				if(h.collider == right.GetComponent<BoxCollider>()) {
					gameObject.SetActive(false);
					mc.SwapRight();
					rm.GoRight();
				}
			}
		}
	}
}