﻿using UnityEngine;
using System.Collections;

public class CameraFollow : MonoBehaviour {
	[HideInInspector]
	public Transform toFollow;

	void Update() {
		if(toFollow != null) {
			Vector3 followPosition = toFollow.position;
			followPosition.z = 0;
			transform.position = followPosition;
		}
	}
}