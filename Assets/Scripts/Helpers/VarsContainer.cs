﻿using UnityEngine;
using System;
using System.Collections;
using System.Linq.Expressions;
using System.Reflection;

[System.Serializable]
public struct Tile {
	public Sprite graphics;
	public float frequency;
}

public enum RoomType {
	None,
	Normal,
	Starting,
	Finishing
}

public enum EnemyType {
	mosquito,
	ship,
	turret,
	ufo,
	walker
}

public class VarsContainer : MonoBehaviour {
	public Tile[] tiles;
	public Weapon[] weapons;

	public GameObject[] enemies;
	public EnemyType[][] roomPrototypes = new EnemyType[10][]
	{
		new EnemyType[] { EnemyType.mosquito, EnemyType.mosquito, EnemyType.mosquito, EnemyType.ship },
		new EnemyType[] { EnemyType.mosquito, EnemyType.mosquito, EnemyType.mosquito, EnemyType.mosquito, EnemyType.mosquito, EnemyType.mosquito },
		new EnemyType[] { EnemyType.ship, EnemyType.turret },
		new EnemyType[] { EnemyType.ship, EnemyType.ship},
		new EnemyType[] { EnemyType.turret, EnemyType.mosquito, EnemyType.mosquito },
		//TODO: implement new types
		new EnemyType[] { EnemyType.mosquito, EnemyType.mosquito, EnemyType.mosquito, EnemyType.ship },
		new EnemyType[] { EnemyType.mosquito, EnemyType.mosquito, EnemyType.mosquito, EnemyType.mosquito, EnemyType.mosquito, EnemyType.mosquito },
		new EnemyType[] { EnemyType.ship, EnemyType.turret },
		new EnemyType[] { EnemyType.ship, EnemyType.ship},
		new EnemyType[] { EnemyType.turret, EnemyType.mosquito, EnemyType.mosquito },
	};

	[HideInInspector]
	public System.Random rand { get; private set; }

	void Awake() {
		//TODO: add seed here
		rand = new System.Random();

		for(int i = 0; i < weapons.Length; i++) {
			switch(weapons[i].type) {
				case WeaponTypes.Single:
					weapons[i] = weapons[i].Reinstance<SingleW>();
					break;
			}
		}
	}
}