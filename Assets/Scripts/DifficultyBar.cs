﻿using UnityEngine;
using System.Collections;

public class DifficultyBar : MonoBehaviour {
	public float min;
	public float max;

	public RectTransform wepColor;
	public RectTransform enemyColor;

	public void SetWepColor(float hue) {
		wepColor.localPosition = new Vector3(Mathf.Lerp(min, max, hue), wepColor.localPosition.y, wepColor.localPosition.z);
	}
	public void SetEnemyColor(float hue) {
		enemyColor.localPosition = new Vector3(Mathf.Lerp(min, max, hue), enemyColor.localPosition.y, enemyColor.localPosition.z);
	}
}