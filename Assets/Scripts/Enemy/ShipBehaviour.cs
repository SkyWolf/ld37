﻿using UnityEngine;
using System.Collections;

public class ShipBehaviour : EnemyBehaviour {
	private Vector3 walkingDirection;
	protected override void EnterState(EnemyState state) {
		if(currentState == EnemyState.Walk) {
			walkingDirection = thePlayer.position - transform.position;
			walkingDirection.z = 0;
			walkingDirection = Quaternion.Euler(0, 0, Random.Range(-10, 10)) * walkingDirection;
			walkingDirection.Normalize();
		}
	}

	private Transform thePlayer;
	protected override void ExitState(EnemyState state) { 
		if(currentState == EnemyState.Spawn)
			thePlayer = GameObject.FindGameObjectWithTag("Player").transform;
	}

	private float timeCollector;
	public float idleDuration = 2;
	public float walkDuration = 1;
	public float walkingSpeed = 1;
	protected override void UpdateState(EnemyState state) {
		switch(currentState) {
		case EnemyState.Spawn:
			ChangeState(EnemyState.Idle);
			break;
		case EnemyState.Idle:
			timeCollector += Time.deltaTime;
			if(timeCollector >= idleDuration) {
				timeCollector = 0;
				ChangeState(EnemyState.Walk);
			}
			break;
		case EnemyState.Walk:
			transform.position += walkingDirection * walkingSpeed * Time.deltaTime;

			timeCollector += Time.deltaTime;
			if(timeCollector >= walkDuration) {
				timeCollector = 0;
				ChangeState(EnemyState.Idle);
			}
			break;
		}
		UpdateLook();
		UpdateShoot();
	}

	public Transform spriteRotator;
	private void UpdateLook() {
		Vector3 lookingDirection = thePlayer.position - transform.position;
		lookingDirection.z = 0;

		spriteRotator.rotation = Quaternion.LookRotation(lookingDirection);
	}

	private float rateOfFireCollector;
	public float rateOfFirePerSecond = 2;
	protected override void UpdateShoot() { 
		rateOfFireCollector += Time.deltaTime;
		if(rateOfFireCollector >= 1 / rateOfFirePerSecond) {
			Vector3 lookingDirection = thePlayer.position - transform.position;
			lookingDirection.z = 0;
			rateOfFireCollector = 0;

			Shoot(lookingDirection.normalized);
		}
	}
}
