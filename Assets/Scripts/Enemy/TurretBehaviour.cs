﻿using UnityEngine;
using System.Collections;

public class TurretBehaviour : EnemyBehaviour {
	private Vector3 walkingDirection;
	protected override void EnterState(EnemyState state) {

	}

	private Transform thePlayer;
	protected override void ExitState(EnemyState state) { 
		if(currentState == EnemyState.Spawn)
			thePlayer = GameObject.FindGameObjectWithTag("Player").transform;
	}

	private float timeCollector;
	protected override void UpdateState(EnemyState state) {
		switch(currentState) {
		case EnemyState.Spawn:
			ChangeState(EnemyState.Idle);
			break;
		}
		UpdateLook();
		UpdateShoot();
	}

	public Transform spriteRotator;
	private void UpdateLook() {
		Vector3 lookingDirection = thePlayer.position - transform.position;
		lookingDirection.z = 0;

		spriteRotator.rotation = Quaternion.LookRotation(lookingDirection);
	}

	private float rateOfFireCollector;
	public float rateOfFirePerSecond = 2;
	protected override void UpdateShoot() { 
		rateOfFireCollector += Time.deltaTime;
		if(rateOfFireCollector >= 1 / rateOfFirePerSecond) {
			Vector3 lookingDirection = thePlayer.position - transform.position;
			lookingDirection.z = 0;
			rateOfFireCollector = 0;

			Shoot(lookingDirection.normalized);
		}
	}
}
