﻿using UnityEngine;
using System.Collections;

public class Colorizer : MonoBehaviour {
	public SpriteRenderer[] renderers;
	public Color thisColor { get; private set; }

	public void ChangeColor(Color c) {
		thisColor = c;
		for(int i = 0; i < renderers.Length; i++)
			renderers[i].color = thisColor;
	}

	public void ChangeColor(float hue) {
		ChangeColor(Color.HSVToRGB(hue, 1, 1));
	}

	public float GetHue() {
		float h,s,v;
		Color.RGBToHSV(thisColor, out h, out s, out v);
		return h;
	}
}
