﻿using UnityEngine;
using System.Collections;
public enum EnemyState {
	Spawn,
	Idle,
	Walk
}
public abstract class EnemyBehaviour : MonoBehaviour {
	public EnemyState currentState;
	public EnemyState lastState;

	protected EnemyParameters eParameters;
	protected Colorizer coloriz;

	public void ChangeState(EnemyState newState) {
		if(newState == currentState)
			return;
		ExitState(currentState);
		currentState = newState;
		EnterState(currentState);
	}

	void Start() {
		eParameters = GetComponent<EnemyParameters>();
		coloriz = GetComponent<Colorizer>();
	}

	void Update() {
		if(eParameters.health <= 0) {
			transform.parent = null;
			Destroy(gameObject);
		}

		UpdateState(currentState);
		UpdateShoot();
	}

	protected abstract void EnterState(EnemyState state);
	protected abstract void ExitState(EnemyState state);
	protected abstract void UpdateState(EnemyState state);
	protected abstract void UpdateShoot();

	public GameObject[] bullets;
	public Transform shootFrom;
	protected virtual void Shoot(Vector3 direction) {
		GameObject bullet = (GameObject)GameObject.Instantiate(bullets[0], shootFrom.position, Quaternion.identity);
		bullet.layer = 10;
		bullet.GetComponent<Colorizer>().ChangeColor(coloriz.thisColor);

		BulletSimple bs = bullet.GetComponent<BulletSimple>();
		bs.Shoot(direction);
	}
}
