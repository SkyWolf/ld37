﻿using UnityEngine;
using System.Collections;

public class MosquitoBehaviour : EnemyBehaviour {
	private Vector3 walkingDirection;
	protected override void EnterState(EnemyState state) {
	}

	private Transform thePlayer;
	protected override void ExitState(EnemyState state) { 
		if(currentState == EnemyState.Spawn)
			thePlayer = GameObject.FindGameObjectWithTag("Player").transform;
	}

	private float timeCollector = float.MaxValue;
	private float angle;
	public float walkDuration = 5;
	public float walkingSpeed = 1;
	protected override void UpdateState(EnemyState state) {
		switch(currentState) {
		case EnemyState.Spawn:
			ChangeState(EnemyState.Walk);
			break;
		case EnemyState.Walk:
			walkingDirection = thePlayer.position - transform.position;
			walkingDirection.z = 0;

			timeCollector += Time.deltaTime;
			if(timeCollector >= walkDuration) {
				timeCollector = 0;
				angle = Random.Range(-30, 30);
			}

			walkingDirection = Quaternion.Euler(0, 0, angle) * walkingDirection;
			walkingDirection.Normalize();

			transform.position += walkingDirection * walkingSpeed * Time.deltaTime;
			break;
		}
	}

	protected override void UpdateShoot () { }
}
