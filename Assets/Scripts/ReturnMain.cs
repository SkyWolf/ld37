﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class ReturnMain : MonoBehaviour {
	void Update () {
		if(Input.anyKeyDown) {
			SceneManager.LoadScene("main", LoadSceneMode.Single);
		}
	}
}
