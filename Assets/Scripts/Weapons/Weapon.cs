﻿using System;
using UnityEngine;

public enum WeaponTypes {
	Single
}

[Serializable]
public class Weapon
{
	public WeaponTypes type;

	public string name = "name";
	public Sprite powerUpGraphics;

	public GameObject[] bullets;

	public float rateOfFirePerSecond;

	private float timeCollector = float.MaxValue;
	private bool canShoot = false;

	public void Update(Color c, Vector3 position, Vector3 direction) {
		timeCollector += Time.deltaTime;
		if(timeCollector >= 1 / rateOfFirePerSecond) {
			canShoot = true;
		}

		if(canShoot && Input.GetKey(KeyCode.Mouse0)) {
			Shoot(c, position, direction);

			canShoot = false;
			timeCollector = 0;
		}
	}

	protected virtual void Shoot(Color c, Vector3 position, Vector3 direction) { }

	public T Reinstance<T>() where T : Weapon, new() {
		T newTypeWep = new T();
		newTypeWep.type = type;
		newTypeWep.name = name;
		newTypeWep.powerUpGraphics = powerUpGraphics;
		newTypeWep.bullets = bullets;
		newTypeWep.rateOfFirePerSecond = rateOfFirePerSecond;

		return newTypeWep;
	}
}