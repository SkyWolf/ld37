﻿using UnityEngine;
using System.Collections;

public class DungeonCreator : MonoBehaviour {
	public int defaultDungeonWidth, defaultDungeonHeight;
	public int defaultRoomWidth, defaultRoomHeight;
	[Range(0, 0.3f)]
	public float erosionFactor;

	public Room[,] dungeon { get; private set; }
	public int width { get; private set; }
	public int height { get; private set; }

	private VarsContainer vars;
	private MeshFilter myMeshFilter;

	private MapCreator theMap;
	public GameObject roomPrefab;

	public int finalInLastColumns = 2;

	void Start() {
		CreateDungeon(defaultDungeonWidth, defaultDungeonHeight);
	}

	private void Initialize () {
		GameObject varsGO = GameObject.FindGameObjectWithTag("Global");
		if(!varsGO)
			Debug.LogError("No \"Global\" object found!");
		else {
			vars = varsGO.GetComponent<VarsContainer>();
			if(!vars) {
				Debug.LogError("\"Global\" had no \"VarsContainer\"");
			}
		}

		GameObject mapGO = GameObject.FindGameObjectWithTag("Map");
		if(!mapGO)
			Debug.LogError("No \"Map\" object found!");
		else {
			theMap = mapGO.GetComponent<MapCreator>();
			if(!theMap) {
				Debug.LogError("\"Map\" had no \"MapCreator\"");
			}
		}
	}

	private void CreateDungeon(int width, int height) {
		if(!vars)
			Initialize();

		this.width = width;
		this.height = height;
		dungeon = new Room[width,height];
		for(int i = 0; i < width; i++) {
			for(int j = 0; j < height; j++) {
				dungeon[i,j] = new Room();
				dungeon[i,j].roomType = RoomType.Normal;
			}
		}

		int startHeight = vars.rand.Next(1, height - 1);
		dungeon[0, startHeight].roomType = RoomType.Starting;

		dungeon[width - vars.rand.Next(1, 1 + finalInLastColumns), vars.rand.Next(1, height - 1)].roomType = RoomType.Finishing;

		//TODO: fix this, i suspect it makes it bug out
		/*for(int i = 0; i < width; i++) {
			if(dungeon[i, 0].roomType == RoomType.Normal) {
				ErodeRoom(i, 0, width, height, erosionFactor);
			}
		}
		for(int i = 0; i < width; i++) {
			if(dungeon[i, height - 1].roomType == RoomType.Normal) {
				ErodeRoom(i, height - 1, width, height, erosionFactor);
			}
		}
		for(int j = 0; j < height; j++) {
			if(dungeon[0, j].roomType == RoomType.Normal) {
				ErodeRoom(0, j, width, height, erosionFactor);
			}
		}
		for(int j = 0; j < height; j++) {
			if(dungeon[width - 1, j].roomType == RoomType.Normal) {
				ErodeRoom(width - 1, j, width, height, erosionFactor);
			}
		}*/

		for(int i = 0; i < width; i++) {
			for(int j = 0; j < height; j++) {
				dungeon[i,j].InitializeRoom((i >= width - finalInLastColumns));
			}
		}

		{
			GameObject nRoom = (GameObject)(GameObject.Instantiate(roomPrefab, Vector3.zero, Quaternion.identity));

			nRoom.name = "The room";

			Transform nRoomT = nRoom.transform;
			RoomCreator nRoomRC = nRoom.GetComponent<RoomCreator>();
			RoomManager nRoomMN = nRoom.GetComponent<RoomManager>();

			nRoomT.parent = transform;
			nRoomT.localPosition = Vector3.zero;
			nRoomRC.CreateRoom(defaultRoomWidth, defaultRoomHeight);
			nRoomMN.Initialize(this, 0, startHeight);
		}

		theMap.CreateMap(dungeon, width, height);
	}

	private void ErodeRoom(int x, int y, int width, int height, float eFactor) {
		if(vars.rand.NextDouble() < eFactor && !LeavingVoid(x, y, width, height)) {
			dungeon[x,y].roomType = RoomType.None;

			if(x > 0) {
				ErodeRoom(x - 1, y, width, height, eFactor / 2);
			}
			if(x < width - 1) {
				ErodeRoom(x + 1, y, width, height, eFactor / 2);
			}
			if(y > 0) {
				ErodeRoom(x, y - 1, width, height, eFactor / 2);
			}
			if(y < height - 1) {
				ErodeRoom(x, y + 1, width, height, eFactor / 2);
			}
		}
	}

	private bool LeavingVoid(int x, int y, int width, int height) {
		if(x > 0 && enumerateNeighbors(x - 1, y, width, height) < 2) {
			return true;
		}
		if(x < width - 1 && enumerateNeighbors(x + 1, y, width, height) < 2) {
			return true;
		}
		if(y > 0 && enumerateNeighbors(x, y - 1, width, height) < 2) {
			return true;
		}
		if(y < height - 1 && enumerateNeighbors(x, y + 1, width, height) < 2) {
			return true;
		}
		return false;
	}

	private int enumerateNeighbors(int x, int y, int width, int height) {
		int nCount = 0;
		if(x > 0 && dungeon[x - 1, y].roomType != RoomType.None)
			nCount++;
		if(x < width - 1 && dungeon[x + 1, y].roomType != RoomType.None)
			nCount++;
		if(y > 0 && dungeon[x, y - 1].roomType != RoomType.None)
			nCount++;
		if(y < height - 1 && dungeon[x, y + 1].roomType != RoomType.None)
			nCount++;

		return nCount;
	}
}